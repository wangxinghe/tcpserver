package au.com.philology.tcpserver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import au.com.philology.common.JavaTheme;
import javax.swing.SwingUtilities;

/**
 *
 * @author ricolwang
 */
public class TcpServerDemo
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        JavaTheme.setLookAndFeel(JavaTheme.LOOKANDFEEL_MOTIF);
        // TODO code application logic here
        SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                new FrameMain().setVisible(true);
            }
        });
    }

}
